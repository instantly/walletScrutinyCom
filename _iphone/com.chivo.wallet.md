---
wsId: ChivoWallet
title: Chivo Wallet
altTitle: 
authors:
- danny
appId: com.chivo.wallet
appCountry: sv
idd: 1581515981
released: 2021-09-07
updated: 2022-06-02
version: 2.1.0
stars: 2.6
reviews: 3965
size: '46099456'
website: https://chivowallet.com
repository: 
issue: 
icon: com.chivo.wallet.jpg
bugbounty: 
meta: ok
verdict: obfuscated
date: 2021-10-10
signer: 
reviewArchive: 
twitter: chivowallet
social:
- https://www.facebook.com/ChivoWalletSLV

---

{% include copyFromAndroid.html %}
