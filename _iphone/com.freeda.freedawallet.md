---
wsId: freeda
title: Freeda Wallet
altTitle: 
authors:
- danny
appId: com.freeda.freedawallet
appCountry: us
idd: 1545428547
released: 2021-03-17
updated: 2022-05-25
version: 2.2.3
stars: 4.7
reviews: 82
size: '69094400'
website: https://www.freeda.io/
repository: 
issue: 
icon: com.freeda.freedawallet.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-11-08
signer: 
reviewArchive: 
twitter: FreedaWallet
social: 

---

{% include copyFromAndroid.html %}
